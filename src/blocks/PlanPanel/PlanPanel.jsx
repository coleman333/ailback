import React from 'react';
import styles from './PlanPanel.module.scss';
import { Icon } from 'react-materialize';

const PlanPanel = ({ title, description, price, items, action, choose, disabled }) => {
    return (
        <div className={'col-lg-4 col-md-4 col-sm-12'}>
            <div className={styles.container}>
                <div className={styles.header}>
                    <div className={styles.title}>{title}</div>
                    <div>{description}</div>
                    <div>
                        <span className={styles.price}>{price}</span><span>/month</span>
                    </div>
                    <button disabled={disabled} onClick={() => action(choose)}>SELECT PLAN</button>
                </div>
                <hr />
                <div className={styles.footer}>
                    {items.map(item => <div key={item}>
                        <Icon className={styles.icon}>check</Icon>
                        <span>{item}</span>
                    </div>)}
                </div>
            </div>
        </div>
    );
}

export default PlanPanel;