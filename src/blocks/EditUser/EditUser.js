import React, { Component } from 'react';
import styles from './EditUser.module.scss';
import { Input } from 'react-materialize';
import InputMask from 'react-input-mask';
import { phoneValidator, emailValidator, fieldNotEmpty } from '../../utils/validators';
import Phone from '../../common/Phone';

class EditUser extends Component {
    constructor(props) {
        super(props);
        const { firstName, lastName, phone, email, partner } = this.props.userData ? this.props.userData : {};
        this.state = {
            form: {
                firstName,
                lastName,
                phone,
                email,
                partner
            },
            errors: {}
        }
    }


    cancel = () => {
        this.props.editUserById(null);
    }

    save = (e) => {
        e.preventDefault();
        const isValid = this.isValid();

        if(isValid) {
            this.props.saveBtn(this.state.form);
        }
    }

    handleChange = (e) => {
        const { target } = e;
        const name = e.target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => this.isValid());
    }

    isValid = () => {
        let isValid = true;
        let results = [];
        results.push(this.isInvalid('phone', this.state.form.phone, [phoneValidator, fieldNotEmpty]));
        results.push(this.isInvalid('email', this.state.form.email, [emailValidator, fieldNotEmpty]));

        let errors =  {};

        for(let obj of results) {
            const key = Object.keys(obj)[0];
            const value = obj[key];
            errors = {
                ...errors,
                ...obj
            }
            if(value){
                isValid = false;
            }
        }
        this.setState({
            errors
        });
        return isValid;
    }

    isInvalid = (field, value, validators) => {
        let isValid = false;
        for(let validator of validators) {
            isValid = validator(value);
            if(!isValid) break;
        }

        return { [field]: !isValid };
    }

    handleChangePhone = (value) => {
        this.setState({ 
            ...this.state,
            form: {
                ...this.state.form,
                phone: value
            }
        }, () => this.isValid());
    }


    render() {
        const { firstName, lastName, phone, email, partner} = this.state.form;
        const { errorMessage } = this.props;
        const { errors } = this.state;

      return (
          <form onSubmit={this.save} className={styles.editUserBlock}>
              <div className={styles.dataInput}>
                <div>
                    <Input name="firstName" onChange={this.handleChange} label="* First name" defaultValue={firstName} />
                </div>
                <div>
                    <Input name="lastName" onChange={this.handleChange} label="* Last name" defaultValue={lastName} />
                </div>
                <div className={errors.email ? styles.errorField : ''}>
                    <Input name="email" onChange={this.handleChange} label="* Email" defaultValue={email} />
                </div>
                <div className={errors.phone ? styles.errorField : ''}>
                    <Phone value={phone} onChange={this.handleChangePhone} placeholder="* Phone" />
                    {/* <InputMask mask="999-999-9999" maskChar="" onChange={this.handleChange} defaultValue={phone} >
                        {(inputProps) => <Input {...inputProps} name="phone" label="Phone" defaultValue={phone} />}
                    </InputMask>  */}
                </div>
                <div className={styles.partner}>
                    <Input name='partner' onChange={this.handleChange} type='checkbox' label='Partner' checked={partner}  />
                </div>
              </div>
              <div className={styles.error}>
                  {errorMessage}
              </div>
              <button type="submit" className={styles.saveButton+' btn btn-success'}>Save</button>
              <div className={styles.cancelButton+' btn btn-success'} onClick={this.cancel}>Cancel</div>
          </form>
      );
    }
}

export default EditUser;
