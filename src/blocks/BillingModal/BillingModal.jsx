import React, { PureComponent } from 'react';
import ModalWindow from '../../common/ModalWindow/ModalWindow';
import styles from './BillingModal.module.scss';
import cn from "classnames";
import { Icon, Input, Button} from "react-materialize";
import {
    CardNumberElement,
    CardExpiryElement,
    CardCVCElement,
    injectStripe
} from 'react-stripe-elements';
import api from '../../utils/api';
import { Link } from 'react-router-dom';
import Spinner from '../../common/Spinner';
const stripeKey = process.env.REACT_APP_STRIPE_KEY;
class BillingModal extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
          open: this.props.open,
          step: 0,
          paymentShedule: this.props.choose,
          paymentMethod: 'creditCard',
          isLoading: false,
          paymentError: false,
          invoicedError: false,
          form: {
            card_number: false,
            card_expiry: false,
            card_cvc: false
          }
        };
      }

      choosePaymentShedule = (choose) => {
          this.setState({
              ...this.state,
              paymentShedule: choose,
          })
      };

      choosePaymentMethod = async(chooseMethod) => {
          await this.setState({
            // ...this.state,
            paymentMethod: chooseMethod,
        });
          // console.log('+++++++++++++',this.state.paymentMethod);
      }

      changeStep = (step) => {
        this.setState({
            step
        });
      }

    handleSubmit = (ev) => {
        this.setState({
            isLoading: true,
        });
        ev.preventDefault();
        const { stripe } = this.props;

        if (stripe) {
            stripe
            .createToken()
            .then( async (payload) => {
                const data = {
                    stripe: payload,
                    plan: this.state.paymentShedule
                }
                this.createSubscription(data);
            }).catch(() => {
                this.setState({
                    isLoading: false,
                    paymentError: true,
                })
            });
        } else {
            console.log("Stripe.js hasn't loaded yet.");
            this.setState({
                isLoading: false,
                paymentError: true,
            })
        }
    };

    async handleACHSubmit(ev){
        ev.preventDefault();
        const stripe = window.Stripe(stripeKey);
        const payload = await stripe.createToken({
            bank_account: {
                country: 'US',
                currency: 'usd',
                account_holder_name: 'Noah Martinez',
                account_holder_type: 'individual',
                routing_number: '110000000',
                account_number: '000123456789',
            },
            key: 'pk_test_In4I2ysmUiapDplnWONm1Nv2'
        });
        console.log(payload);
    }

    createSubscription = async (data) => {
        try {
            const { data: charged } = await api.post('api/Subscriptions/charge', data);
            this.setState({
                isLoading: false,
            });

            if(charged && charged.status === 'succeeded') {
                this.changeStep(3);
            } else {
                this.setState({
                    invoicedError: true,
                })
            }
        } catch (error) {
            console.log(error);
            this.setState({
                invoicedError: true,
                isLoading: false,
            })
        }
    }

    termsOfUse = () => {
        if(this.state.paymentShedule === 'trial') {
            this.createSubscription({
                plan: 'trial'
            });
        } else {
            this.changeStep(1);
        }
    }

    stripeElementChange = (element, name) => {
        if (!element.empty && element.complete) {
          this.setState({
              form: {
                  ...this.state.form,
                [name]: true
            }
          });
        } else {
            this.setState({
                form: {
                    ...this.state.form,
                  [name]: false
              }
            });
        }
    }

    render() {
        const { onClose } = this.props;
        const { open, step, paymentShedule, isLoading, paymentError, invoicedError, form } = this.state;

        return (
            <ModalWindow  open={open} onClose={onClose} center>
                <div className={styles.container}>
                    {step === 0 ? <div>
                        <div>
                            <div className={cn(styles.title, "big-size-font")}>Terms of Use</div>
                        </div>
                        <div className={styles.footer}>
                            <span onClick={() => this.changeStep(0)}>CANCEL</span>
                            <button onClick={() => this.termsOfUse()}>AGREE</button>
                        </div>
                    </div> : null}
                    {step === 1 ? <div>
                        <div>
                            <div className={cn(styles.title, "big-size-font")}>Payment Options</div>
                            <div className={styles.body}>
                                <div>
                                    <div>Choose you payment shedule</div>
                                    <ul>
                                        <li onClick={() => this.choosePaymentShedule('quarterly')} className={paymentShedule === 'quarterly' ? styles.active : ''}>Quarterly $5 / quarter</li>
                                        <li onClick={() => this.choosePaymentShedule('annual')} className={paymentShedule === 'annual' ? styles.active : ''}>Annual $3 / year</li>
                                    </ul>
                                </div>
                                <hr />
                                <div>
                                    <div>Choose you payment method</div>
                                    {/*<ul>*/}
                                        {/*<li className={styles.active}>*/}
                                            {/*<Icon left>credit_card</Icon>*/}
                                            {/*<div>*/}
                                                {/*<div>Credit/Debit Card</div>*/}
                                                {/*<span>Pay with Visa, Master Card, AMEX, Maestro and many other cards.</span>*/}
                                            {/*</div>*/}
                                        {/*</li>*/}
                                        {/*<li className={styles.active}>*/}
                                            {/*<Icon left>credit_card</Icon>*/}
                                            {/*<div>*/}
                                                {/*<div>ACH</div>*/}
                                                {/*<span>ACH.</span>*/}
                                            {/*</div>*/}
                                        {/*</li>*/}
                                    {/*</ul>*/}
                                    <ul>
                                        <li onClick={() => this.choosePaymentMethod('creditCard')} className={paymentShedule === 'quarterly' ? styles.active : ''}>Credit/Debit Card</li>
                                        <li onClick={() => this.choosePaymentMethod('ACH')} className={paymentShedule === 'annual' ? styles.active : ''}>ACH</li>
                                    </ul>
                                    <button onClick={() => this.choosePaymentMethod('creditCard')}>Credit/Debit Card</button>
                                    <button onClick={() => this.choosePaymentMethod('ACH')}>ACH</button>

                                </div>
                            </div>
                        </div>
                        <div className={styles.footer}>
                            <button onClick={() => this.changeStep(2)}>CONTINUE</button>
                        </div>
                    </div> : null}
                    {step === 2 ? <div>
                        { this.state.paymentShedule === 'creditCard' && <div>
                            <div className={cn(styles.title, "big-size-font")}>Credit card details</div>
                            <form className={isLoading ? styles.hide : ''} onSubmit={this.handleSubmit}>
                                {paymentError ? <div className={styles.paymentError}>
                                    Unfortunately, the payment was declined.
                                    <br />
                                    Please, check entered card details and repeat payment.</div> : null}
                                {invoicedError ? <div className={styles.paymentError}>
                                    Unfortunately, the Invoiced server is unavailable to proceed with payment process now.
                                    <br />
                                    Please, try again later.</div> : null}
                                <div className={cn(styles.title, "big-size-font")}>
                                    <div className={cn("input-field col s6")}>
                                        <CardNumberElement onChange={(element) => this.stripeElementChange(element, 'card_number')} />
                                        <label className="active">CARD NUMBER</label>
                                    </div>
                                    <div className={styles.cardSecureData}>
                                        <CardExpiryElement onChange={(element) => this.stripeElementChange(element, 'card_expiry')} />
                                        <CardCVCElement onChange={(element) => this.stripeElementChange(element, 'card_cvc')} />
                                    </div>
                                </div>
                                <div className={cn(styles.return, styles.footer)}>
                                    <span onClick={() => this.changeStep(0)}>Return to Previous</span>
                                    <button disabled={(!form.card_number || !form.card_expiry || !form.card_cvc)} type="submit">COMPLETE</button>
                                </div>
                            </form>
                            <Spinner className={cn(styles.spinner, !isLoading ? styles.hide : '')} />
                        </div>}
                        { this.state.paymentMethod === 'ACH' &&
                        <div>
                            <div className={cn(styles.title, "big-size-font")} style={{width: 300}}>ACH Options</div>
                                <div className={styles.body}>

                                {/*<div className={cn(styles.title, "big-size-font")}>Credit card details</div>*/}
                                <form className={isLoading ? styles.hide : ''} onSubmit={this.handleACHSubmit.bind(this)}>
                                    <div className={cn(styles.title, "big-size-font")} style={{height:200,width:200}}>
                                        <div className={cn("input-field col s6")}>
                                            {/*<CardNumberElement onChange={(element) => this.stripeElementChange(element, 'card_number')} />*/}
                                            <Input
                                                placeholder="Placeholder"
                                                name='routing'
                                                label="routing"
                                            />
                                            <label className="active">ROUTING</label>
                                        </div>
                                        <div className={cn("input-field col s6")}>
                                            {/*<CardNumberElement onChange={(element) => this.stripeElementChange(element, 'card_number')} />*/}
                                            <Input placeholder="Placeholder"
                                                   name='account'
                                                   label="bank account"
                                                   />
                                            <label className="active">BANK ACCOUNT</label>
                                        </div>
                                        {/*<div className={styles.cardSecureData}>*/}
                                            {/*<CardExpiryElement onChange={(element) => this.stripeElementChange(element, 'card_expiry')} />*/}
                                            {/*<CardCVCElement onChange={(element) => this.stripeElementChange(element, 'card_cvc')} />*/}
                                        {/*</div>*/}
                                    </div>
                                    <Button type="button" onClick={this.changeStep.bind(this, 1)}>back</Button>
                                    <Button type="submit">submit</Button>
                                </form>
                                <Spinner className={cn(styles.spinner, !isLoading ? styles.hide : '')} />
                            </div>

                        </div>}

                    </div> : null}
                    {step === 3 ? <div>
                        <div className={cn(styles.title, "big-size-font")}>Confirmation</div>
                        <div className={styles.body}>
                            <span>
                                Verification payment process. The key will be generated after successful payment.
                                <br />
                                You may find the key displayed on a dashboard. It could take some time for updates.
                            </span>
                        </div>
                        <div className={styles.footer}>
                            <Link to="/dashboard">
                                <button>Go to Dashboard</button>
                            </Link>
                        </div>
                    </div> : null}
                </div>
            </ModalWindow>
        );
    }
}

export default injectStripe(BillingModal);
