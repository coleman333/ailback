import axios from 'axios';
import { AUTHORIZATION_REQUIRED } from '../constants/main';
import history from '../../utils/history';
import { SET_TEMPORARY_TOKEN } from '../constants/authentification';
import { DELETE_USER_SELF } from '../constants/usersManage';

export default store => next => action => {
	const { payload, type, } = action;
	const SERVER_URL = process.env.REACT_APP_SERVER_URL;
	
	if(type === SET_TEMPORARY_TOKEN) {
		localStorage.setItem('id', payload );
	}

	if(payload && payload.hasOwnProperty('requestMethod')){
		const method = payload.requestMethod;
		const path = payload.requestPath;
		
		delete payload.requestMethod;
		delete payload.requestPath;

		let reqData = JSON.stringify(payload);
		let token = localStorage.getItem('id');

		axios({
			method,
			url: `${SERVER_URL}${path}`,
			data: reqData,
			headers: {
				'Content-Type': 'application/json',
				"Authorization": token
			}
		  }).then(res => {
			const data = res.data; 

			if(type==='LOGIN'){
				localStorage.setItem('id', data.id );
				localStorage.setItem('user', JSON.stringify(data.user));
			}

			if(type==='LOGOUT' || type==='RESET_PASSWORD'){
				localStorage.clear();
			}

			if(type === DELETE_USER_SELF) {
				localStorage.clear();
				history.push('/');
			}

			return next({
				type: type+'_SUCCESS',
				payload: data
			})
		}).catch(function (error) {  
			console.log('ERROR', error, error.response);
			const code = error.response ? error.response.data.error.code : null;
			if(code === AUTHORIZATION_REQUIRED) {
				localStorage.clear();
				history.push('/');
				return next({
					type: AUTHORIZATION_REQUIRED
				});
			}

			return next({
				type: type+'_FAIL', 
				payload: error
			})
		}); 

	} else {
		next(action)
	}
}
