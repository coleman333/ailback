import { combineReducers } from 'redux';   
import usersManage from './usersManage';
import manageModalWindow from './manageModalWindow';
import keycodesManage from './keycodesManage';
import authentification from './authentification';
import manageInfoModal from './manageInfoModal';
import dashboard from './dashboard';
import billing from './billing';

export default combineReducers({   
	usersManage,
	keycodesManage,
	manageModalWindow,
	authentification,
	manageInfoModal,
	dashboard,
	billing
})