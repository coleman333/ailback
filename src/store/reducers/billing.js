import { SUCCESS, FAIL } from '../constants/main';
import { BILLING_ADD_SUBSCRIPTION } from '../constants/billing';

let initialState = {
    subscription: null,
};

export default function billing(state = initialState, action) {
    const { payload, type } = action;  
    switch (type) {
        case BILLING_ADD_SUBSCRIPTION + SUCCESS:
            return { 
                subscription: payload,
            }
        case BILLING_ADD_SUBSCRIPTION + FAIL:
            return { 
                subscription: null,
            }
        default: 
            break;
    } 
    
    return {
        ...state, 
    }
}
