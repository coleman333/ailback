import React from 'react';
import Modal from 'react-responsive-modal';
import styles from './ModalWindow.module.scss';

const ModalWindow = ({ open, onClose, children, center, showCloseIcon }) => {
  return (
    <div>
      <Modal
        open={open} onClose={onClose}
        center={center}
        classNames={{ modal: styles.modal }}
        showCloseIcon={showCloseIcon}
      >
        <div className={styles.body}>
          {children}
        </div>
      </Modal>
    </div>
  );
};

export default ModalWindow;
