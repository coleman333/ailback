import React, { Component } from "react";
import styles from './RegistrationConfirmation.module.scss';
import { Link } from 'react-router-dom';

class RegistrationConfirmation extends Component {

    render(){
        return (
          <div className={styles.confirmPage+' row'}>
              <div className='big-size-font col-md-6 offset-md-3'>
                  Thank you for creating an account.  Please check your email to retrieve your temporary password.
              </div>
              <Link to='/'>
                <button className='btn btn-success'>OK</button>
              </Link>
          </div>
        );

    }
}

export default RegistrationConfirmation;