import React, { Component } from 'react';
import { Input, } from 'react-materialize';
import { emailValidator, } from '../../utils/validators'; 
import styles from './LogIn.module.scss'
import close from '../../img/close.png'

class LogIn extends Component {

    constructor(props) {
      super(props);
      this.state = {
          email: '',
          password: '',
          forgotPwd: false,
      };
        
      this.handleChangeLogin = this.handleChangeLogin.bind(this);
      this.handleSubmitPassword = this.handleSubmitPassword.bind(this);
    }
    
    handleChangeLogin(e) {
      this.setState({email: e.target.value});
    }
        
    handleSubmitPassword(e) {
      this.setState({password: e.target.value});
    }
    
    login = () => {
        const { email, password, } = this.state;
        if(emailValidator(email)){
            const reqData = { email, password, };
            this.props.login(reqData);
        } else {
            alert(' Wrong email format!');
        }
    }
    resetPwd = () => {
        this.setState({
            forgotPwd: !this.state.forgotPwd,
        })
    }

    sendNewPwd = () => {
        const { email, } = this.state;
        this.props.resetPwd({ email })
    }

    render() {
        const { forgotPwd } = this.state;
        const { resetSuccess, resetFail } = this.props;

        return (
            <div className={styles.logInWrapper + ' row' }>

            <div 
                className={styles.close}
                onClick={()=>{this.props.close()}}>
                <img src={close} alt='close' />
            </div>
            
            <div className='col-lg-8 offset-lg-2'>
                <Input 
                    s={12} 
                    label="Login"
                    onChange={this.handleChangeLogin} />
                {
                    resetSuccess ? <span className={styles.successMessage}>Message with recovery instructions was sent to your email. Please, check and follow the instructions.</span>
                    : null
                }
                {
                    resetFail ? <span className={styles.errorMessage}>There is no account associated with this email.</span>
                    : null
                }
            </div>
            
            {forgotPwd ? null :
            <div className='col-lg-8 offset-lg-2'>
                <Input 
                    s={12} 
                    label="Password" 
                    type="password" 
                    onChange={this.handleSubmitPassword} />
            </div>
            }

            <div className={styles.buttons+ ' col-lg-8 offset-lg-2'}>
                <span
                    onClick={this.resetPwd}>{ forgotPwd ? 'Log in' : 'Forgot password?'}</span>
                <div>
                        {forgotPwd ? 
                        (!resetSuccess ? <div 
                            className={styles.button + ' btn btn-success'}
                            onClick={this.sendNewPwd}>Send new password</div> 
                        : null) :
                        <div 
                            className={styles.button + ' btn btn-success'}
                            onClick={this.login}>Log in</div>
                        }
                    <div 
                        className={styles.button + ' btn btn-success'}
                        onClick={()=>{this.props.close()}}>Close</div>
                </div>
            
            </div>
            
            </div>
        );
    }
}

export default LogIn;
