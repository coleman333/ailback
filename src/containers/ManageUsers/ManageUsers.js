import React, { Component } from 'react';
import { connect, } from 'react-redux';
import styles from './ManageUsers.module.scss';
import UserItem from '../../blocks/UserItem/UserItem';
import EditUser from '../../blocks/EditUser/EditUser';
import { editUserById, getAllUsers, addNewUser, saveEditUser, setDeleteUserId, } from '../../store/AC/usersManage' 
import { showModalWindow, } from '../../store/AC/manageModalWindow';


class ManageUsers extends Component {

    componentDidMount() {
        this.props.getAllUsers();
    }

    showAddUserform = () => {
        this.props.editUserById('addNew')
    }

    addNewUser = (data) => {
        this.props.addNewUser(data);
    }

    saveEditUser = (data) => {
        this.props.saveEditUser(this.props.editableUserId, data);
    }
    
    deleteUser = (id) => {
        this.props.makeInactiveUserById(id);
    }

    getUsers = () => {
        const users = this.props.users;
        return users.map(user=><UserItem deleteUserById={this.deleteUser} save={this.saveEditUser} userData={user} key={user.id} {...this.props} />)
    }
  render() {
    const { editableUserId } = this.props;
    
    return (
        <div className={styles.usersPage + ' row'}>
            <p className='col-lg-10 col-md-10 offset-lg-1 offset-md-1 big-size-font'>Manage users</p>
            
            <div className='col-lg-10 col-md-10 offset-lg-1 offset-md-1'>
                
                {editableUserId === 'addNew' ? <EditUser {...this.props} saveBtn={this.addNewUser} />
                :<div className={styles.button+ ' btn btn-sm btn-primary'} onClick={this.showAddUserform}> Add new user</div>}
            </div>
            

            <div className={styles.usersList + ' col-lg-10 offset-lg-1 col-md-12'}>
                <div className={styles.usersListTitle}>
                    <span>First name</span>
                    <span>Last name</span>
                    <span className={styles.email}>Email</span>
                    <span className={styles.phone}>Phone</span>
                    <span>Partner</span>
                    <div>Actions</div>
                </div>

                {this.getUsers()}

            </div>
            
        </div>
    );
  }
}

const mapStateToProps = (state) => {
    const { editableUserId, users, errorMessage } = state.usersManage;
    return {
        editableUserId,
        users,
        errorMessage,
    };
};
  
const mapDispatchToProps = dispatch => ({
  editUserById: id => dispatch(editUserById(id)),
  showModalWindow: modalData => dispatch(showModalWindow(modalData)),
  getAllUsers: () => dispatch(getAllUsers()),
  addNewUser: (data) => dispatch(addNewUser(data)),
  saveEditUser: (id, data) => dispatch(saveEditUser(id, data)),
  makeInactiveUserById: (id) => dispatch(setDeleteUserId(id))
});
  
export default connect(mapStateToProps, mapDispatchToProps)(ManageUsers);