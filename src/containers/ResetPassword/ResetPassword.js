import React, { Component } from "react";
import { connect } from 'react-redux';
import { setTemporaryToken, resetPassword, } from '../../store/AC/authentification';
import { Input, } from 'react-materialize';
import styles from './ResetPassword.module.scss';

class ResetPassword extends Component {

    constructor(props) {
      super(props);
      this.state = {
        newPassword: '',
        pwdSecond: '',
      };
        
      this.handleChangePwd = this.handleChangePwd.bind(this);
      this.handleChangePwdSecond = this.handleChangePwdSecond.bind(this);
    }
    handleChangePwd(e){
        this.setState({
            newPassword: e.target.value
        })
    }
    handleChangePwdSecond(e){
        this.setState({
            pwdSecond: e.target.value
        })
    }
    resetPwd = () => {
        const { newPassword, pwdSecond, } = this.state;
        const { resetPassword, history, } = this.props;
        if(newPassword!==pwdSecond){
            alert('Not same...');
            return;  
        } 
        resetPassword({newPassword});
        history.push('/');
    }
    componentDidMount(){
        const { history:{ location }, setTemporaryToken, } = this.props;
        let pathNameArray = location.pathname.split('/');
        let tempToken = pathNameArray[pathNameArray.length-1];
        setTemporaryToken(tempToken);
    }
    render() {
      return (
        <div className={styles.resetPwd}>
          <p className='big-size-font'>Reset password</p>

          <div>
            <Input 
                s={12} 
                label="Password"
                onChange={this.handleChangePwd} />
            <Input 
                s={12} 
                label="Repeat password"
                onChange={this.handleChangePwdSecond} />
          </div>

            <button 
                className='btn btn-primary'
                onClick={this.resetPwd}>OK</button>
        </div>
      );
    }
}

// const mapStateToProps = (state) => {
//   const { openModal, modalTitle, modalDescription, } = state.manageModalWindow;
//   return {
//     openModal, modalTitle, modalDescription,
//   };
// };

const mapDispatchToProps = dispatch => ({
  setTemporaryToken: token => dispatch(setTemporaryToken(token)),
  resetPassword: password => dispatch(resetPassword(password)),
});

export default connect(null, mapDispatchToProps)(ResetPassword);
// export default ResetPassword;