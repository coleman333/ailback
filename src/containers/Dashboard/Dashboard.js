
import React, { Component } from 'react';
import styles from './Dashboard.module.scss';
import { connect } from 'react-redux';
import { dashboardGetAssignedKeycodes } from '../../store/AC/dashboard';
import ActivationTile from '../../blocks/ActivationTile/ActivationTile';
import TileLayout from '../../common/TileLayout/TileLayout';
import GetStartedTile from '../../blocks/GetStartedTile/GetStartedTile';
import PageLayout from '../../common/PageLayout/PageLayout';

class Dashboard extends Component {
    componentDidMount = () => {
      this.props.getKeycodes();
    };
    
    state = {
        showRightBlock: true,
    }

    switchVisibleRightBlock = () => {
        this.setState({
            showRightBlock: !this.state.showRightBlock,
        })
    }

  render() {
    const { showRightBlock } = this.state;
    const { assignedKeycodes } = this.props;
    return (
        <PageLayout title="My Dashboard">
                {assignedKeycodes.map((keycode, i) => (
                    <TileLayout key={i} >
                        <ActivationTile keycode={keycode}/>
                    </TileLayout>
                ))}

                {showRightBlock ? 
                    <TileLayout>
                        <GetStartedTile switchVisibleRightBlock={this.switchVisibleRightBlock} />
                    </TileLayout>
                :null}
        </PageLayout>
    );
  }
}

const mapStateToProps = (state) => {
    const { assignedKeycodes } = state.dashboard;
    return {
        assignedKeycodes,
    };
};
  
const mapDispatchToProps = dispatch => ({
    getKeycodes: () => dispatch(dashboardGetAssignedKeycodes())
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);