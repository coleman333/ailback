import React, { Component } from 'react';
import styles from './Documentation.module.scss';
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import SampleApps from '../SampleApps/SampleApps';

class Documentation extends Component {
  state = {
    showMenu: true,
  }
  
  showSubMenu = () => {
    if(window.innerWidth<768){
      this.setState({
        showMenu: !this.state.showMenu,
      });
    }
  }
  componentWillMount(){
    const self = this;
    
    if(window.innerWidth<768){
      this.setState({ showMenu : false })
    }
    window.addEventListener('resize', function(){
      if(window.innerWidth<767){
        self.setState({
          showMenu: false,
                isMobile: true,
            })
        } else {
            self.setState({
              showMenu: true,
            })
        }
    })
  }
  
  render() {
    const linkAct = styles.activeLink;
    const { showMenu } = this.state;
    const styleMenuBtn = showMenu ? { 'left': '160px'} : { 'left': '-40px'};
    return (
      <Router>
        <div className={styles.docContainer}>
          <div className='row no-gutters'>
            <div className={styles.subMenu} style={styleMenuBtn} onClick={this.showSubMenu}>{showMenu? 'Hide' : 'Show'} sub menu</div>
            {showMenu ? 
              <menu className='col-lg-3 col-md-3'>
                <ul>
                  <NavLink to='/documentation/welcome' activeClassName={linkAct}>
                    <li onClick={this.showSubMenu}>Welcome</li>
                  </NavLink>
                  <NavLink to='/documentation/sample-apps' activeClassName={linkAct}>
                    <li onClick={this.showSubMenu}>Examples</li>
                  </NavLink>
                  <NavLink to='/documentation/sdk-documentation' activeClassName={linkAct}>
                    <li onClick={this.showSubMenu}>SDK Documentation</li>
                  </NavLink>
                  <NavLink to='/documentation/mobile-imager' activeClassName={linkAct}>
                    <li onClick={this.showSubMenu}>Getting Started With<br/>Mobile Imager</li>
                  </NavLink>
                  <NavLink to='/documentation/installing-demo' activeClassName={linkAct}>
                    <li onClick={this.showSubMenu}>Installing the Aila Demo App</li>
                  </NavLink>
                  <NavLink to='/documentation/interactive-kiosk' activeClassName={linkAct}>
                    <li onClick={this.showSubMenu}>Getting Started With<br/>Interactive Kiosk</li>
                  </NavLink>
                  <NavLink to='/documentation/product-sheets' activeClassName={linkAct}>
                    <li onClick={this.showSubMenu}>Product Spec Sheets</li>
                  </NavLink>
                </ul>
              </menu>
            :null}
            <div className={styles.dockBlock+' col-lg-9 offset-lg-3 col-md-9 offset-md-3 col-xs-10 offset-xs-1'}>
              <div className={styles.oneDocItem}>
              <Route exact path='/documentation/welcome' component={Welcome} />
              <Route path='/documentation/sdk-documentation' component={SdkDoc} />
              <Route path='/documentation/mobile-imager' component={MobileImg} />
              <Route path='/documentation/installing-demo' component={InstallingDemo} />
              <Route path='/documentation/interactive-kiosk' component={InteraktiveKiosk} />
              <Route path='/documentation/product-sheets' component={ProductSheets} />
              <Route path='/documentation/sample-apps' component={SampleApps} />
              </div>
            </div>
        </div>
      </div>
    </Router>
    );
  }
}

export default Documentation;

const Welcome = () => {
  return (
    <div>
    <p className={styles.docTitle}>Welcome</p>
      <p>{fishText}</p>

    <span className={styles.tableName}>Lorem ipsum dolor</span>
    <table className="table table-hover">
    
    <tbody>
      <tr>
        <td>Lorem ipsum dolor</td>
        <td>Lorem ipsum dolor</td>
        <td>Lorem ipsum dolor</td>
      </tr>
      <tr>
        <td>Lorem ipsum dolor</td>
        <td>Lorem ipsum dolor</td>
        <td>Lorem ipsum dolor</td>
      </tr>
      <tr>
        <td>Lorem ipsum dolor</td>
        <td>Lorem ipsum dolor</td>
        <td>Lorem ipsum dolor</td>
      </tr>

    </tbody>
    </table>

  </div>
  )
}


const SdkDoc = () => {
  return(
    <div>
      <p className={styles.docTitle}>SDK Documentation</p>
      <p>{fishText}</p>
    </div>
  )
}

const MobileImg = () => {
  return(
    <div>
      <p className={styles.docTitle}>Getting Started With Mobile Imager</p>
      <p>{fishText}</p>
    </div>
  )
}

const InstallingDemo = () => {
  return(
    <div>
      <p className={styles.docTitle}>Installing the Aila Demo App</p>
      <p>{fishText}</p>
    </div>
  )
}

const InteraktiveKiosk = () => {
  return(
    <div>
      <p className={styles.docTitle}>Getting Started With Interactive Kiosk</p>
      <p>{fishText}</p>
    </div>
  )
}

const ProductSheets = () => {
  return(
    <div>
      <p className={styles.docTitle}> Product Spec Sheets Sample Apps</p>
      <p>{fishText}</p>
    </div>
  )
}

const fishText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse hendrerit, augue et elementum viverra, enim lorem imperdiet nisi, id vestibulum ante nulla eu ipsum. Duis dignissim, massa ut cursus finibus, purus mauris rhoncus eros, quis pulvinar nibh erat at velit. Donec eu luctus mi, at malesuada nunc. Donec pulvinar ipsum nec enim bibendum tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam egestas rhoncus congue. Sed sodales mollis accumsan. Sed in nisi a enim egestas efficitur. Pellentesque efficitur, magna non vulputate pellentesque, tortor nisl luctus lectus, et dictum velit turpis et ex. Sed condimentum arcu egestas neque vehicula, non pellentesque ante interdum. Integer sed blandit quam, vel sodales magna. Vestibulum ut pretium turpis, quis auctor lectus. Sed dui augue, fringilla ut viverra sed, maximus id orci. Suspendisse eros urna, ultricies quis dui aliquam, vehicula cursus odio. Quisque consectetur lacinia libero vel commodo. Maecenas blandit laoreet elit, quis luctus odio euismod ac. Nam eget pharetra magna. Phasellus urna lorem, vulputate elementum aliquet in, tristique at lorem. Mauris suscipit ipsum lacus, nec faucibus magna suscipit id. Nulla volutpat placerat congue. Aliquam pretium dapibus est congue sollicitudin. Vivamus pellentesque ex et ante rhoncus, eu blandit nulla imperdiet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vel lorem sit amet lectus tincidunt posuere placerat nec ex.'
